import uuid
from datetime import datetime

import json, traceback
from functools import wraps
from flask import request, jsonify


class Logger:

    def __init__(self, request_id="***"):
        self.request_id = request_id

    def __get_format__(self, type, str_to_log):
        return "{} | {} | {} | {}".format(self.__get_current_date__(), self.request_id, type, str_to_log)

    def __get_current_date__(self):
        return datetime.now().strftime("%d/%m/%Y @ %H:%M:%S")

    def console(self, type, str_to_log):
        print("{} | {} | {} | {}".format(self.__get_current_date__(), self.request_id, type, str_to_log))

    def event(self, str_to_log):
        print("---------------------------------------------------------------------------------------------")
        print(self.__get_format__("EVENT", str_to_log))
        print("---------------------------------------------------------------------------------------------")
        return self

    def info(self, str_to_log):
        print(self.__get_format__("INFO", str_to_log))
        return self

    def debug(self, str_to_log):
        print(self.__get_format__("DEBUG", str_to_log))
        return self

    def warn(self, str_to_log):
        print(self.__get_format__("WARN", str_to_log))
        return self

    def error(self, str_to_log):
        print(self.__get_format__("ERROR", str_to_log))
        return self


class Response:
    @staticmethod
    def ok_response(msg='Success', data={}, nav=None):
        response = {"code": "00", 'data': data, 'msg': msg, 'system_code': "000"}
        if nav:
            response.update({'nav': nav})
        return jsonify(response), 200

    @staticmethod
    def general_error(msg='Error', code="01", type=None):
        error_details = {"code": code, 'msg': msg, 'data': None, 'system_code': "001"}
        if type:
            error_details.update({'type': type})
        return jsonify(error_details)

    @staticmethod
    def system_error(msg='Error', code="02", type=None):
        error_details = {"code": code, 'msg': msg, 'data': None, 'system_code': "001"}
        if type:
            error_details.update({'type': type})
        return jsonify(error_details)

    @staticmethod
    def input_error(msg="Invalid input provided", code="03", type=None):
        error_details = {"code": code, 'msg': msg, 'data': None, 'system_code': "001"}
        if type:
            error_details.update({'type': type})
        return jsonify(error_details)

    @staticmethod
    def auth_error(msg="Auth Error", code="04", type=None):
        error_details = {"code": code, 'msg': msg, 'data': None, 'system_code': "001"}
        if type:
            error_details.update({'type': type})
        return jsonify(error_details)


def get_storage(description):
    storage = [s for s in description.split(" ") if 'gb' in s]
    return "" if len(storage) < 1 else storage[0]

def get_condition(description):
    return "used" if "used" in description else "new"

def generate_id():
    return str(uuid.uuid4()).replace('-', '')[:32]


class Decorators:
    @staticmethod
    def required_params(*required_parameters):
        """
        Utility function for checking request body parameters flagged as required
        """
        logger = Logger(request_id='***')
        def wrapper(f):
            @wraps(f)
            def wrapped(*args, **kwargs):
                logger.info("request--->{}".format(request))
                logger.info("request.args--->{}".format(request.args))
                logger.info("request.values--->{}".format(request.values))
                logger.info("request.form--->{}".format(request.form))
                logger.info("request.data--->{}".format(request.data))
                try:
                    # extract the json body into a python dict
                    request_data = json.loads(request.data.decode('utf-8'))
                except Exception as e:
                    traceback.print_exc()
                    logger.error("Exception: Malformed JSON Body passed: \n\n{}".format(traceback.format_exc()))
                    return Response.input_error(msg="Malformed JSON Body passed")

                logger.info("Request parameters received: {}".format(request_data))
                logger.info("Parameters required: {}".format(required_parameters))

                # check the parameters received against the paramters specified by the endpoint and store any missing parameters
                missing_params = []
                for param in required_parameters:
                    if param not in request_data:
                        missing_params.append(param)

                # display the missing parameters
                if len(missing_params) > 0:
                    return Response.input_error(msg="The following required parameters are missing: {}".format(missing_params))
                return f(*args, **kwargs)
            return wrapped
        return wrapper