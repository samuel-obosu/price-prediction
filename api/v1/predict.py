import json
import math
import pickle
import traceback
import numpy as np
from flask import Blueprint, request, jsonify
from utils import generate_id, Logger, Decorators, Response, get_storage, get_condition
from config import MODEL_PATH



api = Blueprint('api', __name__, url_prefix='/api/v1')
cat_one_model = pickle.load(open(f'{MODEL_PATH}/cat_one/ensemble.sav', 'rb'))
cat_one_encoder = pickle.load(open(f'{MODEL_PATH}/cat_one/encoder.pkl', 'rb'))


@api.route('/price-prediction', methods=['POST'])
@Decorators.required_params("item_name", "item_brand", "category", "description", "country")
def price_prediction():
	logger = Logger(generate_id()).event("PRICE-PREDICTION")
	try:
		params = json.loads(request.data.decode('utf-8'))
	except Exception as e:
		msg = "Malformed JSON"
		logger.error(msg)
		traceback.print_exc()
		return Response.input_error(msg=msg)

	try:
		prediction = 0
		category = params.get('category')
		if  str(category).strip() == '1':
			prediction = predict_category_one(logger, params)
		return Response.ok_response(data={'price': math.ceil(prediction)})

	except Exception as e:
		msg = "An error occurred"
		logger.error(msg)
		traceback.print_exc()
		return Response.system_error(msg=msg)


def predict_category_one(logger, params):
	try:
		data_set = [[params.get('item_name').lower(), params.get('item_brand').lower(), 
		get_condition(params.get('description').lower()), get_storage(params.get('description').lower()), params.get('country').lower()]]

		logger.info(f"Dataset: {data_set}")
		
		data_hash = cat_one_encoder.transform(data_set)
		prediction = cat_one_model.predict(data_hash)[0]
		logger.info(f"Model prediction: {prediction}")
		return prediction

	except Exception as e:
		msg = "An error occurred"
		logger.error(msg)
		traceback.print_exc()
		return 0	
