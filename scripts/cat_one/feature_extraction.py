import pandas as pd 

data = pd.read_csv('/home/samuel/Documents/users_report_labeled.csv', nrows=4998)
data_list = []
for index, row in data.iterrows():
	name = row['Name']
	#WEIGHTS
	features = {'vowels': 1, 'whitespace':2, 'consonants':10, 'numbers':20}
	whitespace,vowels,consonants,numbers=0,0,0,0
	for char in name:
		if char.strip() == "":
			whitespace += features.get('whitespace')
		elif char.isnumeric():
			numbers += features.get('numbers')
		elif char in ['a', 'e', 'i', 'o', 'u']:
			vowels += features.get('vowels')
		elif char.isalpha():
			consonants += features.get('consonants')

	value = True if int(row['Fraud']) == 1 else False
	data_list.append([vowels, whitespace, consonants, numbers, value])

df = pd.DataFrame(data_list).to_csv('converted_dataset.csv', header=list(features.keys())+['fraud'], sep=",", index=False)
