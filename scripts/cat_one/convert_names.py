
def convert_name(name):
	#WEIGHTS
	features = {'vowels': 1, 'whitespace':2, 'consonants':10, 'numbers':20}
	whitespace,vowels,consonants,numbers=0,0,0,0
	for char in name:
		if char.strip() == "":
			whitespace += features.get('whitespace')
		elif char.isnumeric():
			numbers += features.get('numbers')
		elif char in ['a', 'e', 'i', 'o', 'u']:
			vowels += features.get('vowels')
		elif char.isalpha():
			consonants += features.get('consonants')

	return [vowels, whitespace, consonants, numbers]


test_names = ["flkjeior", "ifjoiek", "Sam", "Kweku Obosu", "Akosua", "Yeboah", "Akosua Yeboah", "Kofi", "Kofi Owusu", "ie0rfj"]
converted_names = []
for name in test_names:
	converted_names.append(convert_name(name))

print(converted_names)
